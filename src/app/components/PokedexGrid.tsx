'use client'
import { useContext, useEffect, useState } from "react";
import { Grid } from "@radix-ui/themes";
import GridItem from "./GridItem";
import { FilteredPokemonContext } from "./PokemonApp";
import { Empty } from "./Empty";


function PokedexGrid() {
  const filtered = useContext(FilteredPokemonContext);

  return (
    
    <Grid columns={{initial: "2", sm: "4", md:"5", lg: "6"}} rows="repeat(2, auto)" gap="2" width="auto" style={{ margin: 0, width: '100%' }}>
      {filtered.length === 0 && <Empty />}
      {filtered.map((pokemon) => (
        <GridItem key={pokemon.id} pokemon={pokemon} />
      ))}
    </Grid>
  );
}

export default PokedexGrid
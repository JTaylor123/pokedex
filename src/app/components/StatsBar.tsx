type StatsBarProps = {
    statName: string;
    value: number;
    maxPoints: number;
}

const StatsBar = ({ statName, value, maxPoints }: StatsBarProps) => {
    const widthPercentage = (value / maxPoints) * 100;
  
    return (
        <div className="flex items-center  mb-2">
            <div className="flex-1 text-right pr-2">{statName}</div>
            <div className="w-3/5 bg-white rounded-full h-2.5">
                <div className="bg-green-500 h-2.5 rounded-full" style={{ width: `${widthPercentage}%` }}></div>
            </div>
            <div className="w-10">{value}</div>
        </div>
        
    );
  };
  
  export default StatsBar;
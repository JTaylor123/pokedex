'use client'
import { MagnifyingGlassIcon } from '@radix-ui/react-icons'
import { TextField } from '@radix-ui/themes'
import { type ChangeEventHandler } from 'react';

export type SearchProps = {
    value: string;
    onChange: ChangeEventHandler<HTMLInputElement>;
}

function Search({onChange, value}: SearchProps) {
    return (
        <TextField.Root placeholder="Search by name, type, id…" onChange={onChange} value={value} className='w-full md:w-1/3 lg:w-1/4'>
            <TextField.Slot>
                <MagnifyingGlassIcon height="24" width="24" />
            </TextField.Slot>
        </TextField.Root>
    )
}

export default Search
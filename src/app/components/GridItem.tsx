'use client'
import { Box, Flex, Heading, ScrollArea} from "@radix-ui/themes"
import Image from "next/image";
import TypeIcons from "./TypeIcons";
import * as Dialog from "@radix-ui/react-dialog";
import { Cross1Icon, InfoCircledIcon } from "@radix-ui/react-icons";
import EvolutionTree from "./EvolutionTree";
import { Pokemon } from "../utils/types";
import StatsBar from "./StatsBar";
import { Fragment } from "react";

type GridItemProps = {
    pokemon: Pokemon;
}

function GridItem({ pokemon }: GridItemProps) {
    const { name, id, sprites, height, weight, abilities, stats, moves } = pokemon;
    const imageUrl = sprites.other.dream_world.front_default;

    return (
        <Dialog.Root>
            <Dialog.Trigger>
                <div className="flex flex-col text-center border-2 border-gray-400 rounded-lg group hover:bg-gray-300 p-2 group cursor-pointer transition duration-200 hover:scale-125 hover:delay-200" >
                    <div className="flex justify-between">
                        <div className="w-9 h-9 rounded-full flex justify-center items-center  bg-black  text-white font-bold">
                            {id}
                        </div>
                        <div className="group-hover:bg-black group-hover:text-white rounded-full">
                            <InfoCircledIcon width="36" height="36" />
                        </div>
                    </div>
                    <div className="flex justify-center items-center">
                        <Image src={imageUrl} width={100} height={100} alt={`Pokemon ${name}`} className="object-contain h-[150px] w-[150px]"/>
                    </div>
                    <Heading as="h1">{name}</Heading>
                    <TypeIcons types={pokemon.types} />
                </div>
            </Dialog.Trigger>
            <Dialog.Portal>
                <Dialog.Overlay className="fixed inset-0 bg-black/50"/>
                <Dialog.Content className="fixed left-1/2 top-1/2 -translate-x-1/2 -translate-y-1/2 rounded-md bg-white p-5 md:p-8 text-black shadow w-full max-w-2xl max-h-full overflow-y-auto">
                    <div className="flex justify-between">
                        <Dialog.Title className="text-4xl">
                                {name}
                        </Dialog.Title>
                        
                        <Dialog.Close className="IconButton" aria-label="Close">
                            <Cross1Icon width="24" height="24" />
                        </Dialog.Close>
                    </div>
                        <Flex className="mt-2" align="stretch" direction={{initial: "column", sm: "row"}}>
                            <Box width={{initial: "100%", sm: "60%"}} className="p-2">
                                <div className="relative border-2 border-gray-500 rounded-lg">
                                    <div className="flex justify-center items-center p-5">
                                        <Image src={imageUrl} width={100} height={100} alt={`Pokemon ${name}`} className="object-contain h-[250px] w-[250px]"/>
                                    </div>
                                    <div className="absolute bottom-3 left-3 w-9 h-9 rounded-full flex justify-center items-center  bg-black  text-white font-bold">
                                        {id}
                                    </div>
                                </div>
                            </Box>
                            <Box width={{initial: "100%", sm: "40%"}} className="p-2">
                                <div className="bg-gray-300 p-2 rounded-lg h-full flex items-center justify-center">
                                    <div className="flex flex-col text-center w-full">
                                        <div className="flex justify-around">
                                            <div className="mb-4 flex flex-col">
                                                <p className="font-bold">Height</p>
                                                <p>{height / 10} m</p>
                                            </div>
                                            <div className="mb-4 flex flex-col">
                                                <p className="font-bold">Weight</p>
                                                <p>{weight / 10} kg</p>
                                            </div>
                                        </div>
                                        <div className="flex flex-col mb-4">
                                            <p className="font-bold">Abilities</p>
                                            {abilities.map((ability, index) => (
                                                <Fragment key={index}>
                                                {ability.ability.name}
                                                {index < abilities.length - 1 && ', '}
                                                </Fragment>
                                            ))}
                                        </div>
                                        <div className="flex flex-col">
                                            <p className="font-bold">Type</p>
                                            <TypeIcons types={pokemon.types} />
                                        </div>
                                    </div>
                                </div>
                            </Box>
                        </Flex> 
                        <Flex align="stretch" direction={{initial: "column-reverse", sm: "row"}}>
                        <Box height="100%" width={{initial: "100%", sm: "25%"}} className="p-2">
                            <div className="bg-gray-300 p-2 rounded-lg h-full">
                                <div className="flex flex-col text-center">
                                    <p className="font-bold">Moves</p>
                                    <div className="py-4">
                                    <ul>
                                    <ScrollArea type="always" scrollbars="vertical" radius="full" style={{ height: 190 }}>
                                    {moves.map((move, index) => (
                                        <li key={index}>{move.move.name}</li>
                                    ))}
                                    </ScrollArea>
                                    </ul>
                                    
                                    </div>
                                </div>
                            </div>
                        </Box>
                        <Box height="100%" width={{initial: "100%", sm: "75%"}} className="p-2">
                            <div className="bg-gray-200 p-2 rounded-lg">
                                <div className="flex flex-col text-center">
                                    <p className="font-bold">Base Stats</p>
                                    <div className="py-4 ">
                                        {stats.map((stat, index) => (
                                            <StatsBar
                                            key={index}
                                            statName={stat.stat.name}
                                            value={stat.base_stat}
                                            maxPoints={200}
                                            />
                                        ))}
                                    </div>
                                </div>
                            </div>
                        </Box>
                        </Flex>
                        <Box height="100%" width="100%" className="p-2">
                            <div className="relative border-2 border-gray-500 rounded-lg p-2 text-center">
                                <p className="pb-4 font-bold">Evolutions</p>
                                <div className="p-5">
                                    <EvolutionTree pokemon={pokemon} />
                                </div>
                            </div>
                        </Box>
                </Dialog.Content>
            </Dialog.Portal>
        </Dialog.Root>
    );
}

export default GridItem;

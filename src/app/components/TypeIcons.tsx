import { Tooltip } from '@radix-ui/themes';
import Image from 'next/image'

type TypeIconProps = {
    types: {
        type: {
          name: string;
        }
    }[];
}

function TypeIcons({types}: TypeIconProps) {

    const getTypeIconPath = (type: string) => {
        const typeIconPaths: { [key: string]: string } = {
          bug: '/types/bug.svg',
          dark: '/types/dark.svg',
          dragon: '/types/dragon.svg',
          electric: '/types/electric.svg',
          fairy: '/types/fairy.svg',
          fighting: '/types/fighting.svg',
          fire: '/types/fire.svg',
          flying: '/types/flying.svg',
          ghost: '/types/ghost.svg',
          grass: '/types/grass.svg',
          ground: '/types/ground.svg',
          ice: '/types/ice.svg',
          leaf: '/types/leaf.svg',
          normal: '/types/normal.svg',
          poison: '/types/poison.svg',
          psychic: '/types/psychic.svg',
          rock: '/types/rock.svg',
          steel: '/types/steel.svg',
          water: '/types/water.svg',
        };
    
        return typeIconPaths[type];
    };

    return (
        <div className="flex justify-center py-2">
          {types.map((type, index) => (
            <Tooltip content={type.type.name} key={index}>
              <div className='px-2'>
                <Image
                  key={index}
                  src={getTypeIconPath(type.type.name)}
                  width={30}
                  height={30}
                  alt={`${type.type.name} type`}
                />
              </div>
            </Tooltip>
          ))}
        </div>
    );
}

export default TypeIcons
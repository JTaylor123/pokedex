'use client'
import { ChangeEventHandler, createContext, useCallback, useMemo, useState, useEffect } from "react";
import { Container, Heading } from "@radix-ui/themes";
import Search from "./Search";
import PokedexGrid from "./PokedexGrid";
import { PokemonPre, type Pokemon } from "../utils/types";

export const PokemonContext = createContext<Pokemon[]>([]);
export const FilteredPokemonContext = createContext<Pokemon[]>([]);

export type PokemonAppProps = {
    pokemonPre: PokemonPre[];
}

export function PokemonApp({ pokemonPre }: PokemonAppProps) {

    const [pokemon, setPokemon] = useState<Pokemon[]>([]);

    useEffect(() => {
        Promise.all(pokemonPre.map(p0 => fetch(p0.url))).then(responses => {
            Promise.all(responses.map(r0 => r0.json())).then(data => {
                setPokemon(data);
            })
        })        
    }, [pokemonPre]);

    const [search, setSearch] = useState('');

    const handleSearch: ChangeEventHandler<HTMLInputElement> = useCallback((e) => {
        setSearch(e.target.value);
    }, [setSearch]);

    const filtered = useMemo(() => {
        if (search.length > 0) {
            return pokemon.filter(p => p.name.startsWith(search) || 
                p.types.some(type => type.type.name.startsWith(search) ||
                p.id.toString().startsWith(search)
            ));
        }
        return pokemon;
    }, [search, pokemon]);

    return (
        <PokemonContext.Provider value={pokemon}>
            <FilteredPokemonContext.Provider value={filtered}>
                <Container className="px-2">
                    <div className="md:flex md:justify-between py-6 md:py-14 md:flex-row">
                        <Heading className="mb-4 md:mb-0">POKEDEX</Heading>
                        <Search onChange={handleSearch} value={search} />
                    </div>
                        <PokedexGrid />
                </Container>
            </FilteredPokemonContext.Provider>
        </PokemonContext.Provider>
    )
}
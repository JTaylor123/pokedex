import { Flex } from "@radix-ui/themes";
import Image from "next/image";
import { Fragment, useContext, useEffect, useState } from "react";
import { Pokemon } from "../utils/types";
import { PokemonContext } from "./PokemonApp";
import { ArrowRightIcon } from "@radix-ui/react-icons";

type EvolutionTreeProps = {
    pokemon: Pokemon
}

function EvolutionTree({ pokemon }: EvolutionTreeProps) {
    const [pokemonEvolution, setPokemonEvolution] = useState<any>(null);
  
    const pokemonMany = useContext(PokemonContext);

    useEffect(() => {
      fetch(pokemon.species.url)
        .then((response) => response.json())
        .then((data) => fetch(data.evolution_chain.url))
        .then((response) => response.json())
        .then((data) => setPokemonEvolution(data));
    }, [pokemon.species.url]);
  
    if (!pokemonEvolution) {
      return null; 
    }
  
    const { chain } = pokemonEvolution;

    const renderEvolutionChain = (chain: any) => {
        if (!chain) {
          return null;
        }
      
        const { species, evolves_to } = chain;
        const imageUrl = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/${species.url.split('/').slice(-2)[0]}.svg`;
      
        return (
          <Flex key={species.name} className="space-x-4 items-center" justify={"center"} direction={{initial: "column", sm: "row"}}>
           <div className="flex flex-col text-center">
            <Image src={imageUrl} width={100} height={100} alt={species.name} className="object-contain h-[150px] w-[150px]"/>
            {species.name}
           </div>
            
            {evolves_to && evolves_to.length > 0 && <ArrowRightIcon width="32" height="32" className="rotate-90 sm:rotate-0" />}
      
            {evolves_to &&
              evolves_to.map((evolution: any) => (
                <Fragment key={evolution.species.name}>
                  {renderEvolutionChain(evolution)}
                </Fragment>
              ))}
          </Flex>
        );
      };
  
    return <>{renderEvolutionChain(chain)}</>;

    
  }


export default EvolutionTree
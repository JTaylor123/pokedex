import {Heading} from '@radix-ui/themes';
import Image from 'next/image';

export function Empty() {
    return <div className="flex flex-col col-start-1 md:col-start-2 lg:col-start-3 col-span-2 place-content-center">
         <Image src="/prof_oak.jpeg" width={200} height={300} alt="professor oak" className="m-auto "/>
         <Heading size="3">No Pokemon Found. Gotta catch &apos;em all.</Heading>
    </div>
}
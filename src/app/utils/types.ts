export type Pokemon = {
    id: number;
    name: string;
    sprites: {
        other: {
            dream_world:{
                front_default: string;
            }
        }
    };
    types: {
        type: {
        name: string;
        };
    }[];
    abilities: {
        ability: {
        name: string;
        };
    }[];
    moves: {
        move:{
            name: string;
        }
    }[],
    species: {
        name: string;
        url: string;
    },
    stats: {
        base_stat: number;
        stat: {
            name: string;
        }
    }[]
    height: number;
    weight: number;
};

export type PokemonPre = {
    name: string;
    url: string;
}
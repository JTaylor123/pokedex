import { fetchPokemon } from "./utils/api";
import { PokemonApp } from "./components/PokemonApp";


export default async function Home() {

  const pokemonPre = await fetchPokemon();

  return (
    <main className="flex flex-col justify-center text-center">
      <PokemonApp pokemonPre={pokemonPre} />
    </main>
  );
}

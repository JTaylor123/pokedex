## About

This is a modern looking pokedex using the pokeapi: https://pokeapi.co/api/v2.

It uses React, Radix-UI componenents, TailwindCSS, Next.js

## Getting Started

Once cloned, run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
# or
bun dev
```